# XeCalc

Бэкенд для проекта xecalc.ru


### Как запустить проект

Чтобы запустить XeCalc, выполните следующую команду:

```bash
docker-compose up
```

Запуск тестов:

```bash
docker compose -f docker-compose-test.yml run --rm tests
```

Запуск консольного интерфейса:

```bash
docker compose -f docker-compose-dev.yml run --rm sources
```
