CREATE TABLE IF NOT EXISTS "brand" (
	"id" bigint GENERATED ALWAYS AS IDENTITY NOT NULL UNIQUE,
	"name" text NOT NULL,
	"slug" text NOT NULL UNIQUE,
	PRIMARY KEY ("id")
);

CREATE TABLE IF NOT EXISTS "product" (
	"id" bigint GENERATED ALWAYS AS IDENTITY NOT NULL UNIQUE,
	"name" text NOT NULL,
	"slug" text NOT NULL UNIQUE,
	"brand_id" bigint,
	PRIMARY KEY ("id"),
	FOREIGN KEY ("brand_id") REFERENCES "brand"("id")
);

CREATE TABLE IF NOT EXISTS "product_portion" (
	"id" bigint GENERATED ALWAYS AS IDENTITY NOT NULL UNIQUE,
	"name" text NOT NULL,
	"for_weighing" boolean NOT NULL,
	"calories" numeric(8,2) NOT NULL,
	"fat" numeric(8,2) NOT NULL,
	"protein" numeric(8,2) NOT NULL,
	"carbohydrates" numeric(8,2) NOT NULL,
	"product_id" bigint NOT NULL,
	PRIMARY KEY ("id"),
	FOREIGN KEY ("product_id") REFERENCES "product"("id")
);


ALTER TABLE "product_portion" ADD CONSTRAINT "product_portion_unique" UNIQUE ("product_id", "name");