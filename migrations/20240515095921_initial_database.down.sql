ALTER TABLE "product_portion" DROP CONSTRAINT IF EXISTS "product_portion_unique";
DROP TABLE IF EXISTS "brand" CASCADE;
DROP TABLE IF EXISTS "product" CASCADE;
DROP TABLE IF EXISTS "product_portion" CASCADE;