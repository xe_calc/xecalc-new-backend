package parser_product

import (
	"encoding/json"
	"log"
	"xe_calc_diary/internal"

	"xe_calc_diary/internal/entity"
)

type ImportProductUseCase struct {
	internal.ImportProductRepository
}

func NewImportProductUseCase(repo internal.ImportProductRepository) *ImportProductUseCase {
	return &ImportProductUseCase{repo}
}

func (useCase *ImportProductUseCase) Run(logger *log.Logger, input []byte) (err error) {
	product := &entity.ImportProduct{}

	if err = json.Unmarshal(input, product); err != nil {
		logger.Printf("Ошибка обработки json - %v", err)
		return
	}

	logger.Printf("Обработка сообщения - %+v", product)
	err = useCase.CreateProduct(product, logger)
	return
}
