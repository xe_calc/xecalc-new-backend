package parser_product

import (
	"strings"
	"testing"
	infr_mocks "xe_calc_diary/infrastructure/mocks"
	"xe_calc_diary/internal/repository/mocks"
)

const body = `{
	"Name": "Бобы",
	"Manufacturer": "Hortex",
	"PortionName": "100 г",
	"Carbohydrates": 7.8,
	"Calories": 73,
	"Fat": 0.5,
	"Protein": 6.8
}`

func TestParserProductUseCase(t *testing.T) {
	loggerMock, buf := infr_mocks.LoggerMock()
	repository := mocks.MockRepository{}

	useCase := NewImportProductUseCase(&repository)
	err := useCase.Run(loggerMock, []byte(body))

	if !repository.IsCalled {
		t.Error("Метод создания продукта не был вызыван")
	}

	if err != nil {
		t.Errorf("Ошибка при сохранении продукта - %v", strings.Split(buf.String(), ","))
	}
}

func TestParserProductUseCaseWithRepoError(t *testing.T) {
	loggerMock, _ := infr_mocks.LoggerMock()
	repository := mocks.MockRepository{ReturnError: true}

	useCase := NewImportProductUseCase(&repository)
	err := useCase.Run(loggerMock, []byte(body))

	if !repository.IsCalled {
		t.Error("Метод создания продукта не был вызыван")
	}
	if err == nil {
		t.Errorf("Ошибка репозитория необработана")
	}
}
