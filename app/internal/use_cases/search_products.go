package use_cases

import (
	"log"
	"xe_calc_diary/internal"
	"xe_calc_diary/internal/dto"
)

type SearchProductsUseCase struct {
	repo internal.SearchProductsRepository
}

func NewSearchProductsUseCase(repo internal.SearchProductsRepository) *SearchProductsUseCase {
	return &SearchProductsUseCase{repo}
}

func (useCase *SearchProductsUseCase) Run(logger *log.Logger, input *dto.ProductSearchInput) (
	out *dto.ProductSearchOutput, err error) {
	return useCase.repo.Search(input, logger)
}
