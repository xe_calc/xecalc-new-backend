package internal

import (
	"log"
	"xe_calc_diary/internal/dto"
	"xe_calc_diary/internal/entity"
)

type ImportProductRepository interface {
	CreateProduct(inputProduct *entity.ImportProduct, logger *log.Logger) error
}

type SearchProductsRepository interface {
	Search(query *dto.ProductSearchInput, logger *log.Logger) (*dto.ProductSearchOutput, error)
}

type UseCase[I any] interface {
	Run(logger *log.Logger, input I) error
}

type UseCaseWithOutput[I any, O any] interface {
	Run(logger *log.Logger, input I) (O, error)
}
