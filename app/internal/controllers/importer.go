// Package controllers Регистрация обработчиков kafka, на каждый топик свой обработчик
package controllers

import (
	"log"
	"xe_calc_diary/infrastructure"
	"xe_calc_diary/infrastructure/elasticsearch"
	"xe_calc_diary/internal"
	"xe_calc_diary/internal/repository"
	"xe_calc_diary/internal/use_cases/parser_product"
)

func ConfigureProductImportRoute(router *map[string]internal.UseCase[[]byte], logger *log.Logger) {
	pg, err := infrastructure.NewPostgresFromConfig(logger)
	if err != nil {
		logger.Fatalf("Неудалось настроить подключение к postgres - %v", err)
	}

	elastic, err := elasticsearch.ConfigureElasticsearch()
	if err != nil {
		logger.Fatalf("Неудалось настроить подключение к elasticsearch - %v", err)
	}

	repo := repository.NewImportProductRepositoryPostgres(pg, elastic)
	(*router)["import_product"] = parser_product.NewImportProductUseCase(repo)
}
