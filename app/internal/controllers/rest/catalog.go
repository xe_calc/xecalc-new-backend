package rest

import (
	"log"
	"xe_calc_diary/infrastructure"
	"xe_calc_diary/infrastructure/elasticsearch"
	"xe_calc_diary/infrastructure/rest_api/schemes"
	"xe_calc_diary/internal/dto"
	"xe_calc_diary/internal/repository"
	"xe_calc_diary/internal/use_cases"
)

type CatalogController struct {
	pg  *infrastructure.Postgres
	el  elasticsearch.ElasticInfrastructure
	log *log.Logger
}

func NewCatalogController(pg *infrastructure.Postgres, el elasticsearch.ElasticInfrastructure,
	log *log.Logger) *CatalogController {
	return &CatalogController{pg: pg, el: el, log: log}
}

func (c CatalogController) SearchProducts(request *schemes.ProductSearchRequest) (
	response *schemes.ProductSearchResponse, err error) {
	response = &schemes.ProductSearchResponse{}
	// Если фраза поиска пустая, то возвращаем пустой ответ
	if request.Phrase == "" {
		return
	}

	// Если смещение равно нулю, то устанавливаем его по умолчанию
	if request.Offset > 200 || request.Limit > 100 {
		return
	}

	// Если лимит равен нулю, то устанавливаем его по умолчанию
	if request.Limit == 0 {
		request.Limit = 10
	}

	repo := repository.NewSearchProductsRepositoryDB(c.pg, c.el)
	useCase := use_cases.NewSearchProductsUseCase(repo)
	result, err := useCase.Run(c.log,
		&dto.ProductSearchInput{Phrase: request.Phrase, Limit: request.Limit, Offset: request.Offset})

	if err != nil {
		c.log.Printf("Не удалось обработать запрос на поиск продуктов - %v", err)
		return
	}
	products := make([]*schemes.ProductScheme, 0, len(result.Products))
	for _, product := range result.Products {
		products = append(products, &schemes.ProductScheme{
			Uuid: product.Uuid, Name: product.Name, Brand: product.Manufacturer, Slug: product.Slug})
	}
	next, back := c.hasPagination(result.Total, request.Limit, request.Offset)
	response = &schemes.ProductSearchResponse{Products: products, Count: result.Total, Next: next, Previous: back}
	return
}

func (c CatalogController) hasPagination(count int, limit int, offset int) (next string, back string) {
	if count > 0 {
		if offset+limit <= count {
			next = "true"
		}
		if offset > 0 {
			back = "true"
		}
	}
	return
}
