package controllers

type ProductSearchRequest struct {
	Phrase string
	Limit  int
	Offset int
}
