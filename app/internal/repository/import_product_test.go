package repository

import (
	"bytes"
	"context"
	"fmt"
	"log"
	"strings"
	"testing"
	"xe_calc_diary/infrastructure"
	"xe_calc_diary/infrastructure/elasticsearch"
	"xe_calc_diary/infrastructure/mocks"
	"xe_calc_diary/internal/entity"

	"github.com/jackc/pgx/v5/pgxpool"
)

func cleanAllTables(pg *infrastructure.Postgres) {
	tables := []string{"product", "brand", "product_portion"}
	sql := fmt.Sprintf("TRUNCATE %v", strings.Join(tables, ","))
	pg.Pool.Query(context.Background(), sql)
}

type ProductPortion struct {
	name          string
	productName   string `db:"product_name"`
	productSlug   string `db:"product_slug"`
	brandName     string `db:"brand_name"`
	brandSlug     string `db:"brand_slug"`
	forWeighing   bool   `db:"for_weighing"`
	carbohydrates float32
	calories      float32
	fat           float32
	protein       float32
}

func getAllProductFromDb(pgPool *pgxpool.Pool) (result []ProductPortion) {
	rows, err := pgPool.Query(
		context.Background(),
		`SELECT 
			pp.name name, pp.for_weighing for_weighing, pp.carbohydrates carbohydrates, 
			pp.calories calories, pp.fat fat, pp.protein protein, p.name product_name, 
			p.slug product_slug, COALESCE(b.name, '') brand_name, COALESCE(b.slug, '') brand_slug 
		FROM product_portion pp 
		JOIN product p ON p.id = pp.product_id 
		LEFT JOIN brand b ON b.id = p.brand_id;`,
	)
	if err != nil {
		panic(err)
	} else {
		defer rows.Close()
		for rows.Next() {
			p := ProductPortion{}
			err := rows.Scan(&p.name, &p.forWeighing, &p.carbohydrates, &p.calories, &p.fat, &p.protein,
				&p.productName, &p.productSlug, &p.brandName, &p.brandSlug)
			if err != nil {
				panic(err)
			} else {
				result = append(result, p)
			}
		}
	}
	return
}

func addProduct(
	t *testing.T,
	name string,
	manufacturer string,
	portion string,
	pg *infrastructure.Postgres,
	el elasticsearch.ElasticInfrastructure,
	logger *log.Logger,
	buf *bytes.Buffer,
) {
	repo := NewImportProductRepositoryPostgres(pg, el)
	inputProduct := entity.ImportProduct{
		Name:          name,
		PortionName:   portion,
		Carbohydrates: 10.5,
		Calories:      10.5,
		Fat:           10.5,
		Protein:       10.5,
		Manufacturer:  manufacturer,
	}
	err := repo.CreateProduct(&inputProduct, logger)

	if err != nil {
		t.Fatalf("Ошибка при сохранении продукта - %v", strings.Split(buf.String(), ","))
	}
}

func TestSuccessCreateProduct(t *testing.T) {
	loggerMock, buf := mocks.LoggerMock()
	name := "Бобы"
	nameSlug := "text_slug_manufacturer"
	manufacturer := "Завод бобов"
	manufacturerSlug := "text_slug"
	portionValue := float32(10.5)
	pg, _ := infrastructure.NewPostgresFromConfig(loggerMock)
	defer cleanAllTables(pg)

	slugManufacturerMock := mocks.SlugifierMock{ExpectedSlug: manufacturerSlug, Word: manufacturer}
	slugManufacturerMock.ActivateWithSuccessResponse()
	defer slugManufacturerMock.DeactivateSlugifierMock()

	slugProductMock := mocks.SlugifierMock{ExpectedSlug: nameSlug, Word: fmt.Sprintf("%v (%v)", name,
		manufacturer)}
	slugProductMock.ActivateWithSuccessResponse()
	defer slugProductMock.DeactivateSlugifierMock()

	elasticSearchMock := mocks.ElasticMock{}

	addProduct(t, name, manufacturer, "100 г", pg, &elasticSearchMock, loggerMock, buf)

	results := getAllProductFromDb(pg.Pool)

	if len(results) == 0 {
		t.Fatal("Не найдено порций")
	}

	if len(results) > 1 {
		t.Error("Добавлено больше 1й порции")
	}

	pp := results[0]
	if pp.productName != name {
		t.Errorf("имя продукта %v, ожидается %v", pp.productName, name)
	}
	if pp.productSlug != nameSlug {
		t.Errorf("slug продукта %v, ожидается %v", pp.productSlug, nameSlug)
	}

	if pp.brandName != manufacturer {
		t.Errorf("название бренда %v, ожидается %v", pp.brandName, manufacturer)
	}
	if pp.brandSlug != manufacturerSlug {
		t.Errorf("slug бренда %v, ожидается %v", pp.brandSlug, manufacturerSlug)
	}

	if !pp.forWeighing {
		t.Error("порция не весовая")
	}

	if pp.calories != portionValue {
		t.Errorf("калории %v, ожидается %v", pp.calories, portionValue)
	}
	if pp.carbohydrates != portionValue {
		t.Errorf("углеводы %v, ожидается %v", pp.carbohydrates, portionValue)
	}
	if pp.protein != portionValue {
		t.Errorf("белки %v, ожидается %v", pp.protein, portionValue)
	}
	if pp.fat != portionValue {
		t.Errorf("жиры %v, ожидается %v", pp.fat, portionValue)
	}
	if !elasticSearchMock.IndexIsCalled {
		t.Error("ElasticSearch не вызван")
	} else if elasticSearchMock.CalledIndexCount != 1 {
		t.Errorf("ElasticSearch вызван %v раз, ожидается %v", elasticSearchMock.CalledIndexCount, 1)
	}
}

func TestWithoutBrand(t *testing.T) {
	loggerMock, buf := mocks.LoggerMock()
	pg, _ := infrastructure.NewPostgresFromConfig(loggerMock)
	defer cleanAllTables(pg)

	slugProductMock := mocks.SlugifierMock{ExpectedSlug: "text_slug", Word: "Бобы"}
	slugProductMock.ActivateWithSuccessResponse()
	defer slugProductMock.DeactivateSlugifierMock()

	elasticSearchMock := mocks.ElasticMock{}

	addProduct(t, "Бобы", "", "100 г", pg, &elasticSearchMock, loggerMock, buf)

	results := getAllProductFromDb(pg.Pool)
	if len(results) == 0 {
		t.Fatal("Не найдено порций")
	}
	if len(results) > 1 {
		t.Error("Добавлено больше 1й порции")
	}

	pp := results[0]

	if pp.brandName != "" {
		t.Errorf("название бренда %v, ожидается %v", pp.brandName, "")
	}
	if !elasticSearchMock.IndexIsCalled {
		t.Error("ElasticSearch не вызван")
	} else if elasticSearchMock.CalledIndexCount != 1 {
		t.Errorf("ElasticSearch вызван %v раз, ожидается %v", elasticSearchMock.CalledIndexCount, 1)
	}
}

func TestBrandExist(t *testing.T) {
	loggerMock, buf := mocks.LoggerMock()
	pg, _ := infrastructure.NewPostgresFromConfig(loggerMock)
	defer cleanAllTables(pg)
	name := "Бобы"
	nameSlug := "text_slug_manufacturer"
	manufacturer := "Завод бобов"
	manufacturerSlug := "text_slug"

	brandSlugMock := mocks.SlugifierMock{ExpectedSlug: manufacturerSlug, Word: manufacturer}
	brandSlugMock.ActivateWithSuccessResponse()
	defer brandSlugMock.DeactivateSlugifierMock()

	slugProductMock := mocks.SlugifierMock{ExpectedSlug: nameSlug, Word: fmt.Sprintf("%v (%v)", name, manufacturer)}
	slugProductMock.ActivateWithSuccessResponse()
	defer slugProductMock.DeactivateSlugifierMock()

	_, err := pg.Pool.Exec(context.Background(), "INSERT INTO brand (name, slug) VALUES ($1, $2);", manufacturer, manufacturerSlug)
	if err != nil {
		panic(err)
	}

	elasticSearchMock := mocks.ElasticMock{}

	addProduct(t, name, manufacturer, "100 г", pg, &elasticSearchMock, loggerMock, buf)

	results := getAllProductFromDb(pg.Pool)
	if len(results) == 0 {
		t.Error("Не найдено порций")
	}
	if len(results) > 1 {
		t.Error("Добавлено больше 1й порции")
	}

	pp := results[0]
	if pp.brandName != manufacturer {
		t.Errorf("название бренда %v, ожидается %v", pp.brandName, manufacturer)
	}
	if pp.brandSlug != manufacturerSlug {
		t.Errorf("название бренда %v, ожидается %v", pp.brandSlug, manufacturerSlug)
	}

	var brandCount int
	pg.Pool.QueryRow(context.Background(), "SELECT COUNT(*) FROM brand;").Scan(&brandCount)

	if brandCount != 1 {
		t.Errorf("количество брендов %v, ожидается %v", brandCount, 1)
	}

	if !elasticSearchMock.IndexIsCalled {
		t.Error("ElasticSearch не вызван")
	} else if elasticSearchMock.CalledIndexCount != 1 {
		t.Errorf("ElasticSearch вызван %v раз, ожидается %v", elasticSearchMock.CalledIndexCount, 1)
	}
}

func TestAddPortionToProduct(t *testing.T) {
	loggerMock, buf := mocks.LoggerMock()
	pg, _ := infrastructure.NewPostgresFromConfig(loggerMock)
	defer cleanAllTables(pg)

	name := "Бобы"
	nameSlug := "test_slug"
	slugProductMock := mocks.SlugifierMock{ExpectedSlug: nameSlug, Word: name}
	slugProductMock.ActivateWithSuccessResponse()
	defer slugProductMock.DeactivateSlugifierMock()

	elasticSearchMock := mocks.ElasticMock{}

	addProduct(t, name, "", "100 г", pg, &elasticSearchMock, loggerMock, buf)
	addProduct(t, name, "", "100 мл", pg, &elasticSearchMock, loggerMock, buf)

	results := getAllProductFromDb(pg.Pool)
	if resCount := len(results); resCount != 2 {
		t.Fatalf("Количество порций %v, ожидается %v", resCount, 2)
	}

	var prodCount int
	pg.Pool.QueryRow(context.Background(), "SELECT COUNT(*) FROM product;").Scan(&prodCount)

	if prodCount != 1 {
		t.Errorf("количество продуктов %v, ожидается %v", prodCount, 1)
	}

	if !elasticSearchMock.IndexIsCalled {
		t.Error("ElasticSearch не вызван")
	} else if elasticSearchMock.CalledIndexCount != 1 {
		t.Errorf("ElasticSearch вызван %v раз, ожидается %v", elasticSearchMock.CalledIndexCount, 1)
	}
}

func TestSlugifierBadResponse(t *testing.T) {
	loggerMock, _ := mocks.LoggerMock()
	pg, _ := infrastructure.NewPostgresFromConfig(loggerMock)
	defer cleanAllTables(pg)

	name := "Бобы"
	nameSlug := "test_slug"
	slugProductMock := mocks.SlugifierMock{ExpectedSlug: nameSlug, Word: name}
	slugProductMock.ActivateWithBadResponse()
	defer slugProductMock.DeactivateSlugifierMock()
	expectedError := "некорректный статус ответа при определении slug, текущий статус 500, ожидаемый 200"

	elasticSearchMock := mocks.ElasticMock{}
	repo := NewImportProductRepositoryPostgres(pg, &elasticSearchMock)

	inputProduct := entity.ImportProduct{
		Name:          name,
		PortionName:   "100 г",
		Carbohydrates: 10.5,
		Calories:      10.5,
		Fat:           10.5,
		Protein:       10.5,
	}
	err := repo.CreateProduct(&inputProduct, loggerMock)
	if err == nil {
		t.Fatal("Нет ошибки при запросе продукта")
	} else if errText := err.Error(); errText != expectedError {
		t.Errorf("Неверный текст ошибки - <%v> ожидается <%v>", errText, expectedError)
	}
	results := getAllProductFromDb(pg.Pool)
	if resCount := len(results); resCount != 0 {
		t.Fatalf("Количество порций %v, ожидается %v", resCount, 0)
	}
	if elasticSearchMock.IndexIsCalled {
		t.Error("ElasticSearch вызван")
	}
}

func TestSlugifierEmptyResponse(t *testing.T) {
	loggerMock, _ := mocks.LoggerMock()
	pg, _ := infrastructure.NewPostgresFromConfig(loggerMock)
	defer cleanAllTables(pg)

	name := "Бобы"
	nameSlug := "test_slug"
	slugProductMock := mocks.SlugifierMock{ExpectedSlug: nameSlug, Word: name}
	slugProductMock.ActivateWithEmptyPayload()
	defer slugProductMock.DeactivateSlugifierMock()
	expectedError := "не удалось извлечь slug"

	elasticSearchMock := mocks.ElasticMock{}
	repo := NewImportProductRepositoryPostgres(pg, &elasticSearchMock)
	inputProduct := entity.ImportProduct{
		Name:          name,
		PortionName:   "100 г",
		Carbohydrates: 10.5,
		Calories:      10.5,
		Fat:           10.5,
		Protein:       10.5,
	}
	err := repo.CreateProduct(&inputProduct, loggerMock)
	if err == nil {
		t.Fatal("Нет ошибки при запросе продукта")
	} else if errText := err.Error(); errText != expectedError {
		t.Errorf("Неверный текст ошибки - %v ожидается \"%v\"", errText, expectedError)
	}
	results := getAllProductFromDb(pg.Pool)
	if resCount := len(results); resCount != 0 {
		t.Fatalf("Количество порций %v, ожидается %v", resCount, 0)
	}
	if elasticSearchMock.IndexIsCalled {
		t.Error("ElasticSearch вызван")
	}
}

func TestSlugifierInvalidResponse(t *testing.T) {
	loggerMock, _ := mocks.LoggerMock()
	pg, _ := infrastructure.NewPostgresFromConfig(loggerMock)
	defer cleanAllTables(pg)

	name := "Бобы"
	nameSlug := "test_slug"
	slugProductMock := mocks.SlugifierMock{ExpectedSlug: nameSlug, Word: name}
	slugProductMock.ActivateWithInvalidPayload()
	defer slugProductMock.DeactivateSlugifierMock()
	expectedError := "ошибка декодирования ответа при определении slug - invalid character 'i' looking for beginning " +
		"of value"

	elasticSearchMock := mocks.ElasticMock{}
	repo := NewImportProductRepositoryPostgres(pg, &elasticSearchMock)
	inputProduct := entity.ImportProduct{
		Name:          name,
		PortionName:   "100 г",
		Carbohydrates: 10.5,
		Calories:      10.5,
		Fat:           10.5,
		Protein:       10.5,
	}
	err := repo.CreateProduct(&inputProduct, loggerMock)
	if err == nil {
		t.Fatal("Нет ошибки при запросе продукта")
	} else if errText := err.Error(); errText != expectedError {
		t.Errorf("Неверный текст ошибки - <%v> ожидается <%v>", errText, expectedError)
	}
	results := getAllProductFromDb(pg.Pool)
	if resCount := len(results); resCount != 0 {
		t.Fatalf("Количество порций %v, ожидается %v", resCount, 0)
	}
	if elasticSearchMock.IndexIsCalled {
		t.Error("ElasticSearch вызван")
	}
}
