package mocks

import (
	"errors"
	"log"
	"xe_calc_diary/internal/entity"
)

type MockRepository struct {
	ReturnError bool
	IsCalled    bool
}

func (rep *MockRepository) CreateProduct(inputProduct *entity.ImportProduct, logger *log.Logger) error {
	rep.IsCalled = true
	if rep.ReturnError {
		return errors.New("ошибка при создании продукта")
	}
	return nil
}
