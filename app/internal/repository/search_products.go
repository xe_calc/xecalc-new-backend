package repository

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5"
	"log"
	"xe_calc_diary/infrastructure"
	"xe_calc_diary/infrastructure/elasticsearch"
	"xe_calc_diary/internal/dto"
	"xe_calc_diary/internal/entity"
)

type SearchProductsRepositoryDB struct {
	pg      *infrastructure.Postgres
	elastic elasticsearch.ElasticInfrastructure
}

func NewSearchProductsRepositoryDB(pg *infrastructure.Postgres,
	es elasticsearch.ElasticInfrastructure) *SearchProductsRepositoryDB {
	return &SearchProductsRepositoryDB{pg, es}
}

func (r *SearchProductsRepositoryDB) Search(inputProduct *dto.ProductSearchInput, logger *log.Logger) (
	out *dto.ProductSearchOutput, err error) {
	searchRes, err := r.elSearch(inputProduct)
	if err != nil {
		logger.Printf("Не удалось выполнить поиск elastic - %v", err)
		return
	}
	products, err := r.pgSelect(searchRes.IDS, logger)
	if err != nil {
		logger.Printf("Ошибка при запросе продуктов из postgres - %v", err)
		return
	}
	out = &dto.ProductSearchOutput{
		Products: products,
		Total:    searchRes.COUNT,
	}
	return
}

func (r *SearchProductsRepositoryDB) elSearch(inputProduct *dto.ProductSearchInput) (
	res *elasticsearch.ElasticSearchResult, err error) {
	shouldStmt := make([]interface{}, 0, 3)
	shouldStmt = append(shouldStmt, map[string]interface{}{
		"match_phrase_prefix": map[string]interface{}{
			"name": map[string]interface{}{
				"query": inputProduct.Phrase,
				"boost": 2,
			},
		},
	})
	shouldStmt = append(shouldStmt, map[string]interface{}{
		"wildcard": map[string]interface{}{
			"name": map[string]interface{}{
				"value": fmt.Sprintf("*%v*", inputProduct.Phrase),
				"boost": 2,
			},
		},
	})
	shouldStmt = append(shouldStmt, map[string]interface{}{
		"match": map[string]interface{}{
			"brand": map[string]interface{}{
				"query": inputProduct.Phrase,
				"boost": 1,
			},
		},
	})
	mustNotStmt := map[string]interface{}{
		"exists": map[string]interface{}{
			"field": "brand",
		},
	}

	return r.elastic.FullTextSearch("products", elasticsearch.ElasticSearchQuery{
		From:        inputProduct.Offset,
		Size:        inputProduct.Limit,
		ShouldStmt:  shouldStmt,
		MustNotStmt: mustNotStmt,
	})
}

func (r *SearchProductsRepositoryDB) pgSelect(ids []string, logger *log.Logger) ([]entity.Product, error) {
	query := "SELECT product.uuid, product.name, product.slug, coalesce(brand.name, '') as manufacturer FROM product " +
		"LEFT JOIN brand ON brand.id = product.brand_id WHERE product.id = ANY($1) " +
		"ORDER BY array_position($1, product.id) ASC"

	rows, err := r.pg.Pool.Query(context.Background(), query, ids)
	if err != nil {
		logger.Printf("Не удалось получить продукты - %v", err)
		return nil, err
	}
	defer rows.Close()

	return pgx.CollectRows(rows, pgx.RowToStructByPos[entity.Product])
}
