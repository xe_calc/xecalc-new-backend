package repository

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v5"
	"log"
	"strconv"
	"strings"
	"xe_calc_diary/infrastructure"
	"xe_calc_diary/infrastructure/elasticsearch"
	"xe_calc_diary/internal/entity"
)

type ImportProductRepositoryPostgres struct {
	*infrastructure.Postgres
	elastic elasticsearch.ElasticInfrastructure
}

func NewImportProductRepositoryPostgres(pg *infrastructure.Postgres,
	es elasticsearch.ElasticInfrastructure) *ImportProductRepositoryPostgres {
	return &ImportProductRepositoryPostgres{pg, es}
}

func (r *ImportProductRepositoryPostgres) CreateProduct(inputProduct *entity.ImportProduct, logger *log.Logger) error {
	var brandId int
	var err error

	if inputProduct.Manufacturer != "" {
		if brandId, err = r.selectOrInsertBrand(inputProduct.Manufacturer); err != nil {
			logger.Printf("Не удалось создать Брэнд %v, ошибка - %v", inputProduct.Manufacturer, err)
		}
	}
	productId, isExist, err := r.selectOrInsertProduct(inputProduct.Name, inputProduct.Manufacturer, brandId)
	if err == nil {
		logger.Printf("Обработан продукт %v", inputProduct.Name)
		if !isExist {
			if err = r.indexProduct(productId, *inputProduct); err != nil {
				logger.Printf("Не удалось индексировать продукт %v, ошибка - %v", inputProduct.Name, err)
			}
		}
		err := r.insertPortionIfNotExist(inputProduct.PortionName, productId, inputProduct.Carbohydrates,
			inputProduct.Calories,
			inputProduct.Fat,
			inputProduct.Protein,
		)
		if err != nil {
			logger.Printf("Не удалось создать порцию %v продукта %v, ошибка - %v",
				inputProduct.PortionName, inputProduct.Name, err)
		}
	} else {
		logger.Printf("Не удалось создать продукт %v, ошибка - %v", inputProduct.Name, err)
		return err
	}
	return err
}

func (r *ImportProductRepositoryPostgres) selectOrInsertProduct(name string, brandName string, brandId int) (id int,
	isExist bool, err error) {
	var nameForSlug, slug string
	if brandName != "" {
		nameForSlug = name + fmt.Sprintf(" (%v)", brandName)
	} else {
		nameForSlug = name
	}

	if slug, err = infrastructure.Slugify(nameForSlug); err != nil {
		return
	} else {
		var barndIdValue sql.NullInt32
		if brandId == 0 {
			barndIdValue = sql.NullInt32{}
		} else {
			barndIdValue = sql.NullInt32{Int32: int32(brandId), Valid: true}
		}

		err = pgx.BeginFunc(context.Background(), r.Pool, func(tx pgx.Tx) error {
			tErr := tx.QueryRow(context.Background(), "SELECT id FROM product WHERE slug=$1", slug).Scan(&id)
			if tErr != nil {
				if errors.Is(tErr, pgx.ErrNoRows) {
					tErr = tx.QueryRow(context.Background(),
						"INSERT INTO product (name, brand_id, slug) VALUES ($1, $2, $3) returning (id)",
						name, barndIdValue, slug,
					).Scan(&id)
				}
			} else {
				isExist = true
			}
			return tErr
		})
	}
	return
}

func (r *ImportProductRepositoryPostgres) selectOrInsertBrand(name string) (id int, err error) {
	var slug string
	if slug, err = infrastructure.Slugify(name); err != nil {
		return
	} else {
		_, err = r.Pool.Exec(context.Background(), "INSERT INTO brand (name, slug) VALUES ($1, $2) ON CONFLICT "+
			"(slug) DO NOTHING", name, slug)
		if err != nil {
			return
		} else {
			err = r.Pool.QueryRow(context.Background(), "SELECT id FROM brand WHERE slug=$1", slug).Scan(&id)
		}
	}
	return
}

func (r *ImportProductRepositoryPostgres) insertPortionIfNotExist(
	name string,
	productId int,
	carbohydrates float32,
	calories float32,
	fat float32,
	protein float32,
) (err error) {
	isWeight := strings.Contains(name, "100 г") || strings.Contains(name, "100 мл")
	_, err = r.Pool.Exec(
		context.Background(),
		"INSERT INTO product_portion (name, product_id, carbohydrates, calories, fat, protein, for_weighing) "+
			"VALUES ($1, $2, $3, $4, $5, $6, $7) ON CONFLICT (product_id, name) DO NOTHING",
		name, productId, carbohydrates, calories, fat, protein, isWeight,
	)
	return
}

func (r *ImportProductRepositoryPostgres) indexProduct(id int, product entity.ImportProduct) (err error) {
	document := struct {
		Id           int    `json:"id"`
		Name         string `json:"name"`
		Manufacturer string `json:"manufacturer"`
	}{
		Id:           id,
		Name:         product.Name,
		Manufacturer: product.Manufacturer,
	}
	err = r.elastic.IndexDocument("products", strconv.Itoa(id), document)
	return
}
