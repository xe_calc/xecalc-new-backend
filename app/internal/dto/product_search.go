package dto

import "xe_calc_diary/internal/entity"

type ProductSearchInput struct {
	Phrase string
	Limit  int
	Offset int
}

type ProductSearchOutput struct {
	Products []entity.Product
	Total    int
}
