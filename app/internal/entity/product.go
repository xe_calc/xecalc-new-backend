package entity

type Product struct {
	Uuid         string
	Name         string
	Slug         string
	Manufacturer string
}

type ProductPortion struct {
	Id            int
	Name          string
	ProductId     int
	Carbohydrates float32
	Calories      float32
	Fat           float32
	Protein       float32
	ForWeighing   bool
}
