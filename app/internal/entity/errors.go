package entity

type ErrorsOutput struct {
	Errors map[string]string
}

func (o ErrorsOutput) HasErrors() (res bool) {
	if len(o.Errors) == 0 {
		res = true
	}
	return res
}
