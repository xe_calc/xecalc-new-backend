package entity

type ImportProduct struct {
	Name          string
	Manufacturer  string
	PortionName   string
	Carbohydrates float32
	Calories      float32
	Fat           float32
	Protein       float32
}
