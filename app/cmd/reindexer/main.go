package main

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v5"
	"log"
	"strconv"
	"xe_calc_diary/infrastructure"
	"xe_calc_diary/infrastructure/elasticsearch"
)

type Product struct {
	Id    int
	Name  string
	Brand string
}

func main() {
	logger := infrastructure.ConfigureLogger("index_updater:")
	logger.Println("Обновление индексов продуктов")
	pg, err := infrastructure.NewPostgresFromConfig(logger)
	if err != nil {
		logger.Fatalf("Неудалось настроить подключение к postgres - %v", err)
		return
	}
	defer pg.Pool.Close()

	el, err := elasticsearch.ConfigureElasticsearch()
	if err != nil {
		logger.Fatalf("Неудалось настроить подключение к elasticsearch - %v", err)
		return
	}

	limit := 200
	page := 0
	for {
		products, err := getProducts(pg, limit, page, logger)
		if err != nil {
			logger.Fatalf("Неудалось получить продукты - %v", err)
		}
		if len(products) == 0 {
			logger.Println("Продукты отсутствуют завершение процесса")
			break
		}
		for _, product := range products {
			err := el.IndexDocument("products", strconv.Itoa(product.Id), product)
			if err != nil {
				logger.Fatalf("Неудалось индексировать продукт - %v", err)
			}
		}
		page++
	}
}

func getProducts(pg *infrastructure.Postgres, limit int, page int, logger *log.Logger) (products []Product, err error) {
	offset := limit * page
	logger.Printf("Получение продуктов limit - %v offset - %v", limit, offset)
	query := fmt.Sprintf("SELECT product.id, product.name, Coalesce(brand.name, '') as brand "+
		"FROM product LEFT JOIN brand ON brand.id = product.brand_id LIMIT %v OFFSET %v", limit, offset)

	rows, err := pg.Pool.Query(context.Background(), query)

	if err != nil {
		logger.Printf("Не удалось получить продукты - %v", err)
	}
	defer rows.Close()

	products, err = pgx.CollectRows(rows, pgx.RowToStructByPos[Product])

	return
}
