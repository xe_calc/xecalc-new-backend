package main

import (
	"xe_calc_diary/infrastructure"
	"xe_calc_diary/internal"
	"xe_calc_diary/internal/controllers"
)

func main() {
	kafkaConfig := &infrastructure.KafkaConfig{}
	router := make(map[string]internal.UseCase[[]byte])

	logger := infrastructure.ConfigureLogger("consumer: ")
	logger.Println("Старт консьюмера")

	controllers.ConfigureProductImportRoute(&router, logger)

	infrastructure.InitConfig("KAFKA", kafkaConfig, logger)

	infrastructure.ConsumeMessages(
		kafkaConfig.TopicName,
		kafkaConfig.Broker,
		// При появлении новых топиков сделать зависимость use case от топика
		router["import_product"],
		logger,
	)
}
