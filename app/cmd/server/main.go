package main

import (
	"xe_calc_diary/infrastructure"
	"xe_calc_diary/infrastructure/rest_api"
)

func main() {
	logger := infrastructure.ConfigureLogger("rest_api:")

	rest_api.Run(logger)
}
