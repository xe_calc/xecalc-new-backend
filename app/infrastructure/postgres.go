package infrastructure

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"
)

const (
	_defaultMaxPoolSize  = 1
	_defaultConnAttempts = 10
	_defaultConnTimeout  = time.Second
)

type Postgres struct {
	maxPoolSize  int
	connAttempts int
	connTimeout  time.Duration

	Builder squirrel.StatementBuilderType
	Pool    *pgxpool.Pool
}

func NewPostgresFromConfig(logger *log.Logger) (*Postgres, error) {
	var pg Postgres

	config := &PostgresConfig{}
	InitConfig("POSTGRES", config, logger)

	if config.MaxPoolSize == 0 {
		pg.maxPoolSize = _defaultMaxPoolSize
	} else {
		pg.maxPoolSize = config.MaxPoolSize
	}

	if config.ConnAttempts == 0 {
		pg.connAttempts = _defaultConnAttempts
	} else {
		pg.connAttempts = config.ConnAttempts
	}

	if config.ConnTimeout == 0 {
		pg.connTimeout = _defaultConnTimeout
	} else {
		pg.connTimeout = time.Duration(config.ConnTimeout)
	}

	pg.Builder = squirrel.StatementBuilder.PlaceholderFormat(squirrel.Dollar)

	poolConfig, err := pgxpool.ParseConfig(config.Url)
	if err != nil {
		return nil, fmt.Errorf("pgxpool.ParseConfig: %w", err)
	}

	poolConfig.MaxConns = int32(pg.maxPoolSize)
	for pg.connAttempts > 0 {
		pg.Pool, err = pgxpool.NewWithConfig(context.Background(), poolConfig)
		if err == nil {
			break
		}

		logger.Printf("Попытка подключения к postgres, попыток осталось: %d", pg.connAttempts)

		time.Sleep(pg.connTimeout)

		pg.connAttempts--
	}

	if err != nil {
		return nil, fmt.Errorf("postgres - NewPostgres - connAttempts == 0: %w", err)
	}

	return &pg, nil
}

// Close -.
func (p *Postgres) Close() {
	if p.Pool != nil {
		p.Pool.Close()
	}
}
