package infrastructure

import (
	"github.com/IBM/sarama"
	"log"
	"os"
	"os/signal"
	"xe_calc_diary/internal"
)

func ConsumeMessages(topic string, broker string, useCase internal.UseCase[[]byte], logger *log.Logger) {
	consumer, err := sarama.NewConsumer([]string{broker}, nil)
	if err != nil {
		logger.Fatalln(err)
	}

	consumer.ResumeAll()
	defer func() {
		if err := consumer.Close(); err != nil {
			logger.Fatalln(err)
		}
	}()

	partitionConsumer, partitionErr := consumer.ConsumePartition(topic, 0, sarama.OffsetNewest)
	if partitionErr != nil {
		logger.Fatalln(err)
	}
	partitionConsumer.Resume()

	defer func() {
		if err := partitionConsumer.Close(); err != nil {
			logger.Fatalln(err)
		}
	}()

	// Отслеживание отключения
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)
	logger.Print("Ожидание сообщений")
	for {
		select {
		case msg := <-partitionConsumer.Messages():
			err := useCase.Run(logger, msg.Value)
			if err != nil {
				logger.Printf("Ошибка обработки сообщения - %v", err)
			}
		case <-signals:
			logger.Print("Shutdown")
		}
	}
}
