package infrastructure

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
)

type Resp struct {
	Slug string `json:"slug"`
}

func Slugify(word string) (string, error) {
	if resp, err := http.Get("https://api.xecalc.ru/api/slugifier/?word=" + url.QueryEscape(word)); err != nil {
		return "", err
	} else {
		if status := resp.StatusCode; status != 200 {
			return "", fmt.Errorf(
				"некорректный статус ответа при определении slug, текущий статус %v, ожидаемый %v",
				status,
				200,
			)
		}
		defer resp.Body.Close()

		data := &Resp{}
		err := json.NewDecoder(resp.Body).Decode(data)
		if err != nil {
			return "", fmt.Errorf("ошибка декодирования ответа при определении slug - %w", err)
		}
		if data.Slug == "" {
			return "", errors.New("не удалось извлечь slug")
		}
		return data.Slug, nil
	}
}
