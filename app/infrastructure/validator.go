package infrastructure

import (
	"github.com/go-playground/locales/ru"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	ru_translations "github.com/go-playground/validator/v10/translations/ru"
)

func NewValidator() (*validator.Validate, error) {
	ru := ru.New()
	uni := ut.New(ru, ru)
	trans, _ := uni.GetTranslator("ru")
	validate := validator.New()
	err := ru_translations.RegisterDefaultTranslations(validate, trans)
	return validate, err
}

/*
func ValidateStruct(validate *validator.Validate, data any) (
	result bool, validationErrors map[string]string, err error) {
	// Общие ошибки валидации
	if err = validate.Struct(data); err != nil {
		var invalidValidationError *validator.InvalidValidationError
		if errors.As(err, &invalidValidationError) {
			return
		}

		// Детектирование ошибок в данных
		validationErrors = make(map[string]string)
		for _, fieldErr := range validator.ValidationErrors {
			validationErrors[err.Field()] = err.Error()
		}
	}
	return
}
*/
