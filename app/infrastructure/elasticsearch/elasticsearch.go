package elasticsearch

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	elastic "github.com/elastic/go-elasticsearch/v8"
	"io"
	"log"
	"xe_calc_diary/infrastructure"
)

type ElasticInfrastructure interface {
	IndexDocument(indexName string, docID string, document any) (err error)
	FullTextSearch(index string, query ElasticSearchQuery) (*ElasticSearchResult, error)
}

type ElasticSearch struct {
	cl  *elastic.Client
	log *log.Logger
}

type fullTextSearch struct {
	Query boolQuery `json:"query"`
}

type boolQuery struct {
	Bool shouldQuery `json:"bool"`
}

type shouldQuery struct {
	Should  []interface{} `json:"should"`
	MustNot interface{}   `json:"must_not"`
}

type searchResultBody struct {
	Hits struct {
		Total struct {
			Value int `json:"value"`
		} `json:"total"`
		Hits []struct {
			Id string `json:"_id"`
		} `json:"hits"`
	} `json:"hits"`
}

func ConfigureElasticsearch() (el *ElasticSearch, err error) {
	logger := infrastructure.ConfigureLogger("elasticsearch:")
	elConfig := &infrastructure.ElasticConfig{}
	infrastructure.InitConfig("ELASTIC", elConfig, logger)
	cfg := elastic.Config{
		Addresses: elConfig.Urls,
		Username:  elConfig.Username,
		Password:  elConfig.Password,
	}
	elClient, err := elastic.NewClient(cfg)
	if err != nil {
		return
	}
	return &ElasticSearch{cl: elClient, log: logger}, nil
}

func (el *ElasticSearch) IndexDocument(indexName string, docID string, document any) (err error) {
	data, marshalErr := json.Marshal(document)
	if marshalErr != nil {
		el.log.Printf("ошибка при сериализации документа - %v", marshalErr)
		return
	}

	el.log.Printf("индексация документа - %v", document)
	res, err := el.cl.Index(
		indexName,
		bytes.NewReader(data),
		el.cl.Index.WithDocumentID(docID), // Указание ID документа
		el.cl.Index.WithRefresh("false"),  // Отложенное обновление индекса
	)

	if err != nil {
		log.Fatalf("ошибка при индексации документа: %s", err)
	}

	if res.IsError() {
		body, _ := io.ReadAll(res.Body)
		log.Printf("ошибка при индексации документа: %s", body)
	} else {
		fmt.Printf("документ проиндексирован, ID: %s\n", docID)
	}
	return
}

func (el *ElasticSearch) FullTextSearch(index string, query ElasticSearchQuery) (res *ElasticSearchResult, err error) {
	preparedQuery := fullTextSearch{
		Query: boolQuery{
			Bool: shouldQuery{
				Should:  query.ShouldStmt,
				MustNot: query.MustNotStmt,
			},
		},
	}

	body, err := json.Marshal(preparedQuery)
	if err != nil {
		el.log.Printf("ошибка при сериализации запроса - %v", err)
		return
	}
	resp, err := el.cl.Search(
		el.cl.Search.WithContext(context.Background()),
		el.cl.Search.WithIndex(index),
		el.cl.Search.WithPretty(),
		el.cl.Search.WithBody(bytes.NewReader(body)),
		el.cl.Search.WithTrackTotalHits(true),
		el.cl.Search.WithSize(query.Size),
		el.cl.Search.WithFrom(query.From),
	)
	if err != nil {
		log.Printf("ошибка выполнения запроса: %v", err)
		return
	}

	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		el.log.Printf("ошибка при поиске документа - статус ответа %v, ответ - %v",
			resp.StatusCode, resp.String())
		err = fmt.Errorf("ошибка при поиске документа - статус ответа %v, ответ - %v",
			resp.StatusCode, resp.String())
		return
	}

	var result searchResultBody
	err = json.NewDecoder(resp.Body).Decode(&result)
	if err != nil {
		el.log.Printf("ошибка при декодировании результатов поиска - %v", err)
		return
	}
	res = &ElasticSearchResult{
		IDS:   []string{},
		COUNT: result.Hits.Total.Value,
	}
	for _, item := range result.Hits.Hits {
		res.IDS = append(res.IDS, item.Id)
	}
	return
}
