package elasticsearch

type ElasticSearchQuery struct {
	From        int
	Size        int
	ShouldStmt  []interface{}
	MustNotStmt interface{}
}

type ElasticSearchResult struct {
	IDS   []string
	COUNT int
}
