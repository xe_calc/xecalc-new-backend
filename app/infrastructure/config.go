package infrastructure

import (
	"github.com/kelseyhightower/envconfig"
	"log"
)

type KafkaConfig struct {
	TopicName string `split_words:"true"`
	Broker    string
}

type PostgresConfig struct {
	Url          string
	MaxPoolSize  int `split_words:"true"`
	ConnAttempts int `split_words:"true"`
	ConnTimeout  int `split_words:"true"`
}

type ApiConfig struct {
	Port             int
	JwtTokenLifeTime int    `split_words:"true"`
	JwtTokenSecret   string `split_words:"true"`
}

type ElasticConfig struct {
	Urls     []string
	Username string
	Password string
}

// InitConfig Получение настроек из переменных окружения
func InitConfig(prefix string, conf interface{}, logger *log.Logger) {
	logger.Printf("Получение настроек %v", prefix)
	err := envconfig.Process(prefix, conf)

	if err != nil {
		logger.Fatal(err.Error())
	}
	logger.Println("Настройки извлечены")
}
