package infrastructure

import (
	"log"
	"os"
)

func ConfigureLogger(prefix string) *log.Logger {
	return log.New(os.Stdout, prefix, log.LstdFlags)
}
