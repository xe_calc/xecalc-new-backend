package mocks

import (
	"errors"
	"xe_calc_diary/infrastructure/elasticsearch"
)

type ElasticMock struct {
	CalledSearchCount int
	CalledIndexCount  int
	SearchIsCalled    bool
	IndexIsCalled     bool
	ReturnError       bool
}

func (r *ElasticMock) FullTextSearch(index string, query elasticsearch.ElasticSearchQuery) (
	res *elasticsearch.ElasticSearchResult, err error) {
	r.SearchIsCalled = true
	r.CalledSearchCount++
	if r.ReturnError {
		err = errors.New("ошибка при поиске документа")
	}
	return
}

func (r *ElasticMock) IndexDocument(indexName string, docID string, document any) (err error) {
	r.IndexIsCalled = true
	r.CalledIndexCount++
	if r.ReturnError {
		err = errors.New("ошибка при индексации документа")
	}
	return
}
