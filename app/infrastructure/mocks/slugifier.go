package mocks

import (
	"fmt"
	"github.com/jarcoal/httpmock"
	"net/url"
)

type SlugifierMock struct {
	ExpectedSlug string
	Word         string
}

func (mock SlugifierMock) ActivateSlugifierMock(reponder httpmock.Responder) {
	httpmock.Activate()
	httpmock.RegisterResponder("GET", fmt.Sprintf("https://api.xecalc.ru/api/slugifier/?word=%v",
		url.QueryEscape(mock.Word)), reponder)
}

func (mock SlugifierMock) DeactivateSlugifierMock() {
	httpmock.DeactivateAndReset()
}

func (mock SlugifierMock) ActivateWithSuccessResponse() {
	resp := httpmock.NewStringResponder(200, `{"slug": "`+mock.ExpectedSlug+`"}`)
	mock.ActivateSlugifierMock(resp)
}

func (mock SlugifierMock) ActivateWithBadResponse() {
	resp := httpmock.NewStringResponder(500, `{"error": "error text"}`)
	mock.ActivateSlugifierMock(resp)
}

func (mock SlugifierMock) ActivateWithEmptyPayload() {
	resp := httpmock.NewStringResponder(200, `{"field": "field"}`)
	mock.ActivateSlugifierMock(resp)
}

func (mock SlugifierMock) ActivateWithInvalidPayload() {
	resp := httpmock.NewStringResponder(200, `invalid`)
	mock.ActivateSlugifierMock(resp)
}
