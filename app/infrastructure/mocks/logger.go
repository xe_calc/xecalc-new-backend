package mocks

import (
	"bytes"
	"log"
)

func LoggerMock() (logger *log.Logger, buf *bytes.Buffer) {
	buf = new(bytes.Buffer)
	logger = log.New(buf, "", log.LstdFlags)
	return
}
