package rest_api

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"xe_calc_diary/infrastructure"
	"xe_calc_diary/infrastructure/elasticsearch"
	"xe_calc_diary/infrastructure/rest_api/schemes"
	"xe_calc_diary/internal/controllers/rest"
)

func addProductCatalogRoutes(rg *gin.RouterGroup, pg *infrastructure.Postgres,
	el elasticsearch.ElasticInfrastructure, l *log.Logger) {
	productCatalog := rg.Group("/products")

	productCatalog.GET("/search/", func(c *gin.Context) { searchHandler(c, pg, el, l) })
}

func searchHandler(ctx *gin.Context, pg *infrastructure.Postgres, el elasticsearch.ElasticInfrastructure,
	logger *log.Logger) {
	var searchQuery schemes.ProductSearchRequest
	if err := ctx.ShouldBindQuery(&searchQuery); err != nil {
		logger.Printf("Не удалось обработать запрос на поиск продуктов - %v", err)
		ctx.JSON(http.StatusNotFound, gin.H{})
		return
	}

	controller := rest.NewCatalogController(pg, el, logger)
	result, err := controller.SearchProducts(&searchQuery)
	if err != nil {
		logger.Printf("Ошибка при поиске продуктов статус 400 - %v", err)
		ctx.JSON(http.StatusBadRequest, gin.H{})
	} else {
		ctx.JSON(http.StatusOK, result)
	}
}
