package rest_api

import (
	"github.com/gin-gonic/gin"
	"log"
	"xe_calc_diary/infrastructure"
	"xe_calc_diary/infrastructure/elasticsearch"
)

func Run(logger *log.Logger) {
	router := gin.Default()

	pg, err := infrastructure.NewPostgresFromConfig(logger)
	if err != nil {
		logger.Fatalf("Неудалось настроить подключение к postgres - %v", err)
	}

	elastic, err := elasticsearch.ConfigureElasticsearch()
	if err != nil {
		logger.Fatalf("Неудалось настроить подключение к elasticsearch - %v", err)
	}

	getRoutes(router, pg, elastic, logger)

	// listen and serve on 0.0.0.0:8080
	if err := router.Run(); err != nil {
		logger.Fatalf("Ошибка работы сервера - %v", err)
	}
}

func getRoutes(router *gin.Engine, pg *infrastructure.Postgres, elastic elasticsearch.ElasticInfrastructure,
	l *log.Logger) {
	apiGroup := router.Group("/api")
	addProductCatalogRoutes(apiGroup, pg, elastic, l)
}
