package schemes

type ProductSearchRequest struct {
	Phrase string `form:"name"`
	Limit  int    `form:"limit"`
	Offset int    `form:"offset"`
}

type ProductScheme struct {
	Uuid  string `json:"uuid"`
	Name  string `json:"name"`
	Brand string `json:"brand_name"`
	Slug  string `json:"slug"`
}

type ProductSearchResponse struct {
	Products []*ProductScheme `json:"products"`
	Count    int              `json:"count"`
	Next     string           `json:"next"`
	Previous string           `json:"previous"`
}
